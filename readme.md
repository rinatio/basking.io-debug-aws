# Test Athena with KMS

## Deploy

    $ aws cloudformation deploy --template ./template.yaml --stack-name cfn-test --region eu-west-2

# Test1

## Copy single file

Put your bucket name there!!!

    $ aws s3 sync parquet/api_id\=1/seen_time_rounded\=2019-01-01-01-00/ s3://cfn-test-s3bucket-17haoc7v88t10/parquet/api_id\=1/seen_time_rounded\=2019-01-01-01-00/

## Run queries

In Athena console:

    ALTER TABLE test_resample.data_test ADD IF NOT EXISTS PARTITION (
        api_id = '1', 
        seen_time_rounded = '2019-01-01-01-00'
    )

then run test query #1

    select a from test_resample.data_test;

then find results in CloudTrail:

    select *
    from test_resample.cloudtrail_logs_cfn_test
    where (eventsource = 'athena.amazonaws.com' 
       or eventsource ='kms.amazonaws.com'
       or eventsource ='s3.amazonaws.com')
       and eventname != 'GetQueryExecution'
    order by eventtime desc;

*3 Decrypt events listed!!*

# Test 2

## Copy 6 files

Put your bucket name there!!!

    $ aws s3 sync parquet/ s3://cfn-test-s3bucket-17haoc7v88t10/parquet/
    
## Run queries

In Athena console:

    ALTER TABLE test_resample.data_test ADD IF NOT EXISTS PARTITION (
        api_id = '1', 
        seen_time_rounded = '2019-01-01-02-00'
    );
    ALTER TABLE test_resample.data_test ADD IF NOT EXISTS PARTITION (
        api_id = '1', 
        seen_time_rounded = '2019-01-01-03-00'
    );
    ALTER TABLE test_resample.data_test ADD IF NOT EXISTS PARTITION (
        api_id = '2', 
        seen_time_rounded = '2019-01-01-01-00'
    );
    ALTER TABLE test_resample.data_test ADD IF NOT EXISTS PARTITION (
        api_id = '2', 
        seen_time_rounded = '2019-01-01-02-00'
    );
    ALTER TABLE test_resample.data_test ADD IF NOT EXISTS PARTITION (
        api_id = '2', 
        seen_time_rounded = '2019-01-01-03-00'
    );

then run test query #2

    with pre AS (
      SELECT  a, b, c, d, api_id, seen_time_rounded
      FROM test_resample.data_test
      WHERE api_id = 2
      and seen_time_rounded >= '2019-01-01-01-00' 
      and seen_time_rounded < '2019-01-01-02-00'
    )
    select * from pre
        where api_id = 2
                  and a >= 410
                  and a < 490
    order by a, seen_time_rounded;

then find results in CloudTrail:

    select *
    from test_resample.cloudtrail_logs_cfn_test
    where (eventsource = 'athena.amazonaws.com' 
       or eventsource ='kms.amazonaws.com'
       or eventsource ='s3.amazonaws.com')
       and eventname != 'GetQueryExecution'
    order by eventtime desc;

*6 Decrypt events listed!!*

3 Decrypts per file in a single partition


# Test 3

Run test query #3

    SELECT max(seen_time_rounded) AS seen_time_rounded
    FROM test_resample.data_test
    WHERE api_id = 1;

then find results in CloudTrail:

    select *
    from test_resample.cloudtrail_logs_cfn_test
    where (eventsource = 'athena.amazonaws.com' 
       or eventsource ='kms.amazonaws.com'
       or eventsource ='s3.amazonaws.com')
       and eventname != 'GetQueryExecution'
    order by eventtime desc;

*12 Decrypt events listed!!*

2 Decrypts for each file in the `api_id=1` partition.
